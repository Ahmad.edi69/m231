# M231 - Datenschutz und Datensicherheit anwenden

Setzt Datenschutz und Datensicherheit bei Informatiksystemen ein. Überprüft vorhandene Systeme auf Einhaltung von Richtlinien.

Eine Übersicht über die im Modul 231 zu erreichenden Kompetenzen finden Sie im Abschnitt [Kompetenzmatrix](00_kompetenzband/)

# Modulinhalt
 - [Datenschutz](01_protection/)
 - [Git / Markdown](02_git)
 - [Passwortverwaltung](03_passwords)
 - [Ablagekonzept](04_filing_system/)
 - [Backup](05_backup)

# Leistungsbeurteilung

Alle Informationen zu der Leistungsbeurteilung finden Sie [hier](/00_evaluation/README.md). 
 
 # Unterlagen
  - Gitrepository [ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)
  - Gitbook [ch-tbz-it.gitlab.io/Stud/m231/](https://ch-tbz-it.gitlab.io/Stud/m231/)
 - Unterlagen die aus urheberrechtlichen Gründen nicht hier veröffentlicht werden können, sind im MS Teams zu finden. 